package actions;

import common.start.Element;
import common.start.LocalVariables;
import common.start.StartParameters;

import java.io.IOException;

public class SetValues {

    public static void setValores(StartParameters sttp, LocalVariables lv, Element el, String nameElement, String attribute, String valueAttribute, String valueField, String actions, String message) throws IOException, InterruptedException {

        lv.setRobotNameElement(nameElement);
        lv.setRobotAttribute(attribute);
        lv.setRobotValueAttribute(valueAttribute);
        lv.setRobotValueField(valueField);
        lv.setRobotActions(actions);
        lv.setRobotMessage(message);
        SelectActions.efetiveActions(sttp, lv, el);
    }

}
