package navigation;

import actions.SetValues;
import common.start.Element;
import common.start.LocalVariables;
import common.start.StartParameters;
import common.utils.Validacao;

public class Carrinho {

    public static void adicionarCarrinho(LocalVariables lv, StartParameters sttp, Element el) throws Exception {


        SetValues.setValores(sttp, lv, el, "adicionarcarrinho", "xpath", el.getAdicionarprodutonocarrinho(), "", "click", "Falha no preenchimento do campo nome");
        Validacao.validacaoCarrinho(lv,sttp, el);
        SetValues.setValores(sttp, lv, el, "adicionarcep", "id", el.getAdicionarcep(), sttp.getTableLine()[1], "sendKeys", "Falha no preenchimento do campo nome");
        SetValues.setValores(sttp, lv, el, "botaookcep", "xpath", el.getBotaookcep(), "", "click", "Falha no preenchimento do campo nome");
        Validacao.validacaoValorFrete(lv,sttp, el);
    }


}
