package navigation;

import actions.SetValues;
import common.start.Element;
import common.start.LocalVariables;
import common.start.StartParameters;
import common.utils.Validacao;


public class HomeSite {


    public static void buscarProduto(LocalVariables lv, StartParameters sttp, Element el) throws Exception {

        SetValues.setValores(sttp, lv, el, "botaoentendi", "xpath", el.getBotaoentendi(), "", "click", "Falha no preenchimento do campo nome");
        SetValues.setValores(sttp, lv, el, "pesquisa", "id", el.getCampopesquisa(), sttp.getTableLine()[0], "sendKeys", "Falha no preenchimento do campo nome");
        SetValues.setValores(sttp, lv, el, "botaopesquisar", "xpath", el.getBotaolupadebusca(), "", "click", "Falha no preenchimento do campo nome");
        Validacao.validacaoProduto(lv,sttp, el);
        SetValues.setValores(sttp, lv, el, "escolherproduto", "xpath", el.getEscolherproduto(), "", "click", "Falha no preenchimento do campo nome");

    }

}
