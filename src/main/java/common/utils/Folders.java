package common.utils;

import common.start.StartParameters;

import java.io.File;

public class Folders {

    public static void generateRootFolder(StartParameters sttp) {

        File dir = new File(sttp.getProp().getProperty("rootfolder") + sttp.getDateNow());
        dir.mkdir();
        sttp.setDir(dir);

    }

    public static void generateSubFolder(StartParameters sttp){

        File dir2 = new File(sttp.getDir() + "\\resultado1");
        dir2.mkdir();
        sttp.setDir2(dir2);
    }
}
