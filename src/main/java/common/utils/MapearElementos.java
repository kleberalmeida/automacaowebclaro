package common.utils;

import common.start.Element;

import java.io.IOException;

public class MapearElementos {

    public static void elementos (Element el) throws IOException, InterruptedException {


        el.setCampopesquisa("inpHeaderSearch");
        el.setBotaolupadebusca("//*[@id=\"btnHeaderSearch\"]/i");
        el.setResultadodabusca("//h1/strong");
        el.setEscolherproduto("//*[@id=\"product_ajb67b5g2d\"]/a[1]/div/img");
        el.setAdicionarprodutonocarrinho("//div[3]/div[2]/button");
        el.setResultadoadicionadocarrinho("//*[@id=\"root\"]/div/div[2]/div/div[1]");
        el.setAdicionarcep("ZipcodeForm-input");
        el.setBotaookcep("//form/button");
        el.setResultadovalorcep("//div[2]/div/div[3]/div/div[2]");
        el.setBotaoentendi("/html/body/div[2]/div[2]/p");




    }


}
