package common.utils;

import actions.SetValues;
import common.start.Element;
import common.start.LocalVariables;
import common.start.StartParameters;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class Validacao {


    public static boolean validacaoProduto(LocalVariables lv, StartParameters sttp, Element el) throws Exception {

        Thread.sleep(3000);
        String valortelevisao = sttp.getDriver().findElement(By.xpath(el.getResultadodabusca())).getText();
        String textotelevisao = "televisao";

        if (valortelevisao.contains(textotelevisao)) {

            return true;

        } else {

            throw new Exception("Exception message");
        }

    }

    public static boolean validacaoCarrinho(LocalVariables lv, StartParameters sttp, Element el) throws Exception {

        Thread.sleep(3000);
        String valorcarrinhho = sttp.getDriver().findElement(By.xpath(el.getResultadoadicionadocarrinho())).getText();
        String textocarrinho = "Sacola";

        if (valorcarrinhho.equals(textocarrinho)) {

            return true;

        } else {

            throw new Exception("Exception message");
        }
    }
        public static boolean validacaoValorFrete(LocalVariables lv, StartParameters sttp, Element el) throws Exception {

            Thread.sleep(3000);
            String valorfrete = sttp.getDriver().findElement(By.xpath(el.getResultadovalorcep())).getText();
            String textofrete = "R$ 36,90";

            if (valorfrete.equals(textofrete)) {

                return true;

            } else {

                throw new Exception("Exception message");
            }

        }

}
