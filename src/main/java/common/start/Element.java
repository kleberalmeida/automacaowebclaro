package common.start;

public class Element {


    private String campopesquisa;
    private String botaolupadebusca;
    private String resultadodabusca;
    private String escolherproduto;
    private String adicionarprodutonocarrinho;
    private String resultadoadicionadocarrinho;
    private String adicionarcep;
    private String botaookcep;
    private String resultadovalorcep;
    private String botaoentendi;


    public String getCampopesquisa() { return campopesquisa; }

    public void setCampopesquisa(String campopesquisa) { this.campopesquisa = campopesquisa; }

    public String getBotaolupadebusca() { return botaolupadebusca; }

    public void setBotaolupadebusca(String botaolupadebusca) { this.botaolupadebusca = botaolupadebusca; }

    public String getResultadodabusca() { return resultadodabusca; }

    public void setResultadodabusca(String resultadodabusca) { this.resultadodabusca = resultadodabusca; }

    public String getEscolherproduto() { return escolherproduto; }

    public void setEscolherproduto(String escolherproduto) { this.escolherproduto = escolherproduto; }

    public String getAdicionarprodutonocarrinho() { return adicionarprodutonocarrinho; }

    public void setAdicionarprodutonocarrinho(String adicionarprodutonocarrinho) { this.adicionarprodutonocarrinho = adicionarprodutonocarrinho; }

    public String getResultadoadicionadocarrinho() { return resultadoadicionadocarrinho; }

    public void setResultadoadicionadocarrinho(String resultadoadicionadocarrinho) { this.resultadoadicionadocarrinho = resultadoadicionadocarrinho; }

    public String getAdicionarcep() { return adicionarcep; }

    public void setAdicionarcep(String adicionarcep) { this.adicionarcep = adicionarcep; }

    public String getBotaookcep() { return botaookcep; }

    public void setBotaookcep(String botaookcep) { this.botaookcep = botaookcep; }

    public String getResultadovalorcep() { return resultadovalorcep; }

    public void setResultadovalorcep(String resultadovalorcep) { this.resultadovalorcep = resultadovalorcep; }

    public String getBotaoentendi() { return botaoentendi; }

    public void setBotaoentendi(String botaoentendi) { this.botaoentendi = botaoentendi; }
}
